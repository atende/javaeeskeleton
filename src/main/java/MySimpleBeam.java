import javax.inject.Named;

/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 10/9/13
 * Time: 11:32 PM
 */
@Named
public class MySimpleBeam {
    public String helloWorld(){
        return "Hello World";
    }
}
